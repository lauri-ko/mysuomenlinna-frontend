import React, { useState } from 'react'

const TableItem = ({date}) => {

  return (
    <td>{ date.getMinutes() }</td>
  )
}

export default TableItem