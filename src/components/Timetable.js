import React from 'react'
import TableItem from './TableItem'

const Timetable = ({ data }) => {

  const dataByHours = data.reduce((acc, cur) => {
    const hour = cur.getHours()
    const last = acc.length - 1
    if(!acc[last]) {
      acc = [[hour, cur]]
      return acc
    } else if(acc[last][0] === hour){
      acc[last].push(cur)
      return acc
    } else {
      acc.push([hour, cur])
      return acc
    }
  }, [])
  console.log("testi", dataByHours)

  const renderRows = () => {
    return dataByHours.map(rows => {
      const first = rows[0]
      const rest = rows.slice(1)
      return(
        <tr>
          <td>{first}</td>
          {rest.map(item => <TableItem date={item}/>)}
        </tr>
      )
    })
  }

  // {data.map(item => <tr><td>{item}</td></tr>)}
  return (
    <table>
      <tbody>
        {renderRows()}
      </tbody>
    </table>
  )
}

export default Timetable