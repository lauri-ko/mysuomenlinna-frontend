import React, { useState, useEffect } from 'react'
import Timetable from './components/Timetable'
import test from './services/hslService'

const App = () => {
  const [data, setData] = useState([])

  useEffect(() => {
    const fetchData = async () => {
      try {
        const data = await test()
        const list = data.data.stop.stoptimesWithoutPatterns
        const stop = data.data.stop.name
        const filtered = list
          .filter(item => item.trip.tripHeadsign !== stop)
          .map(item => {
            const timestamp = (item.scheduledDeparture + item.serviceDay) * 1000
            const date = new Date(timestamp)
            return date
          })
      setData(filtered)
      }
      catch(error){
        console.log(error)
      }
    }
    fetchData()
  }, [])

  return (
    <div className="App">
      <Timetable data={data}/>
    </div>
  )
}

export default App
