
const url = '/'

let date = new Date()
date.setHours(5,0,0,0,)
let timestamp = date.getTime()
console.log(timestamp)
timestamp = (timestamp-(timestamp%1000))/1000
console.log(timestamp)
console.log(date)

let id = 'HSL:1030701'

let query = 
`{
  stop(id: "${id}") {
    name
    stoptimesWithoutPatterns(startTime: ${timestamp}, timeRange: 86400, numberOfDepartures: 200) {
      scheduledDeparture
      serviceDay
      trip {
        tripHeadsign
      }
    }
  }
}`

const test = () => {

  return fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ query })
  })
  .then(response => response.json())

}

export default test